<?php

namespace App\Http\Resources;

use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 * @property string type
 * @property \App\Models\UserTransaction pivot
 * @property \Carbon\Carbon datetime
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => array_search($this->type, Transaction::$types),
            'amount' => $this->pivot ? $this->pivot->amount : 0.0,
            'datetime' => $this->datetime->toDateTimeString(),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
