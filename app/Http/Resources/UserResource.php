<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\Account;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 * @property string email
 * @property string phone
 * @property string address
 * @property string type
 * @property \App\Models\Account account
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @method static \Illuminate\Database\Eloquent\Builder transactions()
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'balance' => $this->transactions()->sum('amount'),
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'type' => array_search($this->type, User::$types),
            'account' => [
                'account_number' => $this->account->account_number,
                'type' => array_search($this->account->type, Account::$types),
                'description' => $this->account->description,
            ],
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
