<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['register', 'login']);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        $user = factory(User::class)->create([
            'name' => $request->post('name'),
            'phone' => $request->post('phone'),
            'address' => $request->post('address'),
            'email' => $request->post('email'),
            'password' => bcrypt($request->post('password'))
        ]);

        $account = factory(Account::class)->make();

        $user->account()->save($account);

        return $this->login($request);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $client = DB::table('oauth_clients')->where('password_client', true)->first();

        $parameters = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $request->post('email'),
            'password' => $request->post('password')
        ];

        $request = Request::create('/oauth/token', 'POST', $parameters);

        $response = app()->handle($request);
        $body = json_decode((string) $response->getContent(), true);
        if (isset($body['access_token'])) {
            $user = User::whereEmail($parameters['username'])->first();
            if ($user) {
                $body['type'] = array_search($user->type, User::$types);

                return response()->json($body);
            }
        }

        return $response;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = $request->user();

        $user->token()->revoke();

        return $this->emptyApiResponse();
    }

    /**
     * Get currently logged in user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return UserResource
     */
    public function user(Request $request)
    {
        $user = $request->user();

        return new UserResource($user);
    }
}
