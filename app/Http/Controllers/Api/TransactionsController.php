<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\TransactionResource;
use Illuminate\Validation\ValidationException;

class TransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get the mutation report.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function mutation(Request $request)
    {
        $user = $request->user();

        return TransactionResource::collection($user->transactions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deposit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required|numeric|min:1',
        ]);

        DB::beginTransaction();

        try {
            $user = $request->user();
            $transaction = factory(Transaction::class)->create([
                'name' => $request->post('name'),
                'type' => Transaction::$types['deposit']
            ]);
            $user->transactions()->attach($transaction->id, [
                'amount' => $request->post('amount')
            ]);
        } catch (\Exception $exception) {
            Log::debug($exception->getTraceAsString());
            DB::rollBack();
        }

        DB::commit();

        return $this->emptyApiResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function withdrawal(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required|numeric|min:1|max:' . $user->transactions()->sum('amount'),
        ]);

        DB::beginTransaction();

        try {
            $transaction = factory(Transaction::class)->create([
                'name' => $request->post('name'),
                'type' => Transaction::$types['withdrawal']
            ]);
            $user->transactions()->attach($transaction->id, [
                'amount' => -$request->post('amount')
            ]);
        } catch (\Exception $exception) {
            Log::debug($exception->getTraceAsString());
            DB::rollBack();
        }

        DB::commit();

        return $this->emptyApiResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function transfer(Request $request)
    {
        $from = $request->user();

        $this->validate($request, [
            'name' => 'required',
            'account_number' => 'required|exists:accounts',
            'amount' => 'required|numeric|min:1|max:' . $from->transactions()->sum('amount'),
        ]);

        if ($request->post('account_number') === $from->account->account_number) {
            throw ValidationException::withMessages([
                'account_number' => 'You are not allowed to transfer to your own account number.'
            ]);
        }

        DB::beginTransaction();

        try {
            $to = User::whereHas('account', function (Builder $query) use ($request) {
                $query->where('account_number', $request->post('account_number'));
            })->first();

            $transaction = factory(Transaction::class)->create([
                'name' => $request->post('name'),
                'type' => Transaction::$types['transfer']
            ]);
            $transaction->users()->attach([
                $from->id => ['amount' => -$request->post('amount')],
                $to->id => ['amount' => $request->post('amount')]
            ]);
        } catch (\Exception $exception) {
            Log::debug($exception->getTraceAsString());
            DB::rollBack();
        }

        DB::commit();

        return $this->emptyApiResponse();
    }
}
