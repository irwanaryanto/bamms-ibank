<?php

namespace App\Http\Controllers\Api;

use App\Models\UserTransaction;
use App\Http\Controllers\Controller;

class BanksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get bank summary.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function summary()
    {
        $data = [
            'name' => 'Bamms iBank',
            'phone' => '+1-623-228-4866',
            'address' => '173 Giovanny Haven Cassandraville, NE 61988',
            'balance' => UserTransaction::query()->sum('amount'),
        ];

        return response()->json(compact('data'));
    }
}
