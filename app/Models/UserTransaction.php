<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\UserTransaction
 *
 * @property int $transaction_id
 * @property int $user_id
 * @property float $amount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTransaction whereUserId($value)
 * @mixin \Eloquent
 */
class UserTransaction extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'user_id', 'amount',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
