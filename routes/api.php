<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', 'Api\AuthController@register');
Route::post('/auth/login', 'Api\AuthController@login');
Route::post('/auth/logout', 'Api\AuthController@logout');
Route::get('/auth/user', 'Api\AuthController@user');

Route::post('/transactions/deposit', 'Api\TransactionsController@deposit');
Route::post('/transactions/withdrawal', 'Api\TransactionsController@withdrawal');
Route::post('/transactions/transfer', 'Api\TransactionsController@transfer');
Route::get('/transactions/mutation', 'Api\TransactionsController@mutation');
Route::get('/bank/summary', 'Api\BanksController@summary');