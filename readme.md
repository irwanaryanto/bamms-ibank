<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Bamms iBank

Full stack Developer Testing

## Requirements
- PHP >= 7.1.3
- Composer
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension
- MySQL >= 5.7

## Installation
- Clone and setup the projects.
```sh
$ git clone https://gitlab.com/irwanaryanto/bamms-ibank.git bamms-ibank
$ cd bamms-ibank
```
- Create mysql database with database name bamms_ibank
```sh
$ mysql -u root -e "create database bamms_ibank";
```
- Install Laravel dependencies
```sh
$ composer install
```
- Update .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=bamms_ibank
DB_USERNAME=root
DB_PASSWORD=change-to-your-mysql-password
```
- Setup environment
```sh
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ php artisan key:generate
$ php artisan migrate --seed
$ php artisan serve
```
- Go to [http://127.0.0.1:8000/login](http://127.0.0.1:8000/login)
- You can login with some sample below.

| Email                 | Password | Role     |
| --------------------- | -------- | -------- |
| manager@example.org   | secret   | Manager  |
| customer1@example.org | secret   | Customer |
| customer2@example.org | secret   | Customer |
| customer3@example.org | secret   | Customer |
| customer4@example.org | secret   | Customer |
| customer5@example.org | secret   | Customer |

- For Api Documentation [Postman](https://documenter.getpostman.com/view/3576315/S11By2Ab)