export default [
    { path: '/', redirect: '/login' },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: require('./components/DashboardComponent').default
    },
    {
        path: '/login',
        name: 'login',
        component: require('./components/LoginComponent').default
    },
    {
        path: '/register',
        name: 'register',
        component: require('./components/RegisterComponent').default
    },
    {
        path: '/manager/dashboard',
        name: 'manager/dashboard',
        component: require('./components/Manager/DashboardComponent').default
    },
];