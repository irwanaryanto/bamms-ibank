import VueRouter from 'vue-router';
import Routes from './routes';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('manager-summary-component', require('./components/Manager/SummaryComponent.vue').default);
Vue.component('summary-component', require('./components/SummaryComponent.vue').default);
Vue.component('deposit-component', require('./components/DepositComponent.vue').default);
Vue.component('withdrawal-component', require('./components/WithdrawalComponent.vue').default);
Vue.component('transfer-component', require('./components/TransferComponent.vue').default);
Vue.component('mutation-component', require('./components/MutationComponent.vue').default);
Vue.component('header-component', require('./components/HeaderComponent.vue').default);
Vue.component('app-component', require('./components/AppComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter);
const router = new VueRouter({
    routes: Routes,
    mode: 'history',
    base: '/',
});

Vue.filter('capitalize', function (value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1)
});

const app = new Vue({
    el: '#app',

    router
});
