<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Transaction::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'type' => \App\Models\Transaction::$types['deposit'],
        'datetime' => now(),
    ];
});
