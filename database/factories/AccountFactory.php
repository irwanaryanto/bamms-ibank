<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Account::class, function (Faker $faker) {
    return [
        'account_number' => $faker->bankAccountNumber,
        'type' => \App\Models\Account::$types['debit'],
        'description' => $faker->sentence,
    ];
});
