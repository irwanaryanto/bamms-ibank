<?php

use App\Models\User;
use App\Models\Account;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::whereEmail('manager@example.org')->doesntExist()) {
            $manager = factory(User::class)->create([
                'email' => 'manager@example.org',
                'type' => User::$types['manager'],
            ]);

            $manager->account()->save(factory(Account::class)->make());
        }

        for ($i = 1; $i <= 5; $i++) {
            $email = 'customer' . $i . '@example.org';
            if (User::whereEmail($email)->doesntExist()) {
                $customer = factory(User::class)->create([
                    'email' => $email
                ]);

                $customer->account()->save(factory(Account::class)->make());
            }
        }
    }
}
