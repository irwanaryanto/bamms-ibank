<?php

use Illuminate\Database\Seeder;

class PassportPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->doesntExist();

        if ($client) {
            Artisan::call('passport:client', [
                '--password' => true,
                '--quiet' => true,
            ]);
            $keys = File::exists(storage_path('oauth-private.key')) && File::exists(storage_path('oauth-public.key'));
            if (!$keys) {
                Artisan::call('passport:keys',  [
                    '--force' => true,
                ]);
            }
        }
    }
}
